import {Order} from "./order_model";

import {DeliveryZone} from "./delivery_zone";
import {sha256} from "js-sha256";
import {TinkoffInputModel} from "./tinkoff_input_model";

const functions = require('firebase-functions');

const admin = require('firebase-admin');
const express = require('express');
const nodemailer = require('nodemailer');
const moment = require('moment');

const testPhone = '+7 (911) 781-16-18';

admin.initializeApp();

const db = admin.firestore();

const acquiringPassword = 'wbomi9lkvuvis01b';

const gmailEmail = 'mospizza.mobile@gmail.com';
const gmailPassword = '12qw34er56ty';
const mailTransport = nodemailer.createTransport({
 service: 'gmail',
 auth: {
  user: gmailEmail,
  pass: gmailPassword,
 },
});

function getMoscowTime() {
 const date = new Date();
 date.setHours(date.getHours() + 3);
 return date;
}

function getSimpleProductFromBonusProduct(catalogBonusProduct, currentTime: Date, totalPrice: number) {
 if (catalogBonusProduct == null) { console.log('catalog bonus product is empty'); return; }

 const currentWeekday = currentTime.getDay() === 0 ? 7 : currentTime.getDay();
 console.log('currentWeekday ' + currentWeekday);
 const weekSchedule: [any] = catalogBonusProduct['week_schedule'];

 if (weekSchedule == null || (weekSchedule.length < 1)) { console.log('week schedule is empty'); return extractDefaultProductFromBonusProduct(catalogBonusProduct, totalPrice); }

 const schedule = weekSchedule.find((rawSchedule) => {
  const timeSchedule = rawSchedule['time_schedule'];
  if (timeSchedule == null) return false;

  if (rawSchedule.weekday !== currentWeekday) return false;

  const startTime: string = timeSchedule.start_time;
  if (startTime == null) return false;

  const endTime: string = timeSchedule.end_time;
  if (endTime == null) return false;

  console.log('start time: ' + moment(startTime, "HH:mm"));
  console.log('end time: ' + moment(endTime, "HH:mm"));

  return moment(currentTime).isBetween(moment(startTime, "HH:mm"), moment(endTime, "HH:mm"));
 });

 if (schedule == null) { console.log('schedule is empty'); return extractDefaultProductFromBonusProduct(catalogBonusProduct, totalPrice); }

 const entity = schedule.time_schedule.entity;

 console.log('retrieved bonus good with conditions');

 const fromFirstClientPrice: number = entity['default_start_from_first_client_price'] || catalogBonusProduct['default_start_from_first_client_price'];
 const fromPrice: number = entity['start_from_price'] || catalogBonusProduct['default_start_from_price'];
 const fromPriceClub: number = entity['start_from_price_club'] || catalogBonusProduct['default_start_from_price_club'];

 // TODO: implement logic with first customer and loyalty card
 const minPrice = Math.min(fromFirstClientPrice, fromPrice, fromPriceClub);
 if (minPrice > totalPrice) return false;

 return {
  'quantity': 1,
  'name': entity['product_name'] || catalogBonusProduct['default_product_name'],
  'category': catalogBonusProduct['category'],
  'size': entity['size'] || catalogBonusProduct['default_size'],
  'additional_ingredients': [],
  'picture': entity['picture'] || catalogBonusProduct['picture'],
  // TODO: fix depends on club card 'default_price_club'
  'price': entity['price'] || catalogBonusProduct['default_price']
 };
}

function extractDefaultProductFromBonusProduct(catalogBonusProduct, totalPrice: number) {
 const defaultName = catalogBonusProduct['default_product_name'];
 if (defaultName == null) return false;

 const fromFirstClientPrice: number = catalogBonusProduct['default_start_from_first_client_price'];
 const fromPrice: number = catalogBonusProduct['default_start_from_price'];
 const fromPriceClub: number = catalogBonusProduct['default_start_from_price_club'];

 // TODO: implement logic with first customer and loyalty card
 const minPrice = Math.min(fromFirstClientPrice, fromPrice, fromPriceClub);
 if (minPrice > totalPrice) return false;

 return {
  'quantity': 1,
  'name': defaultName,
  'category': catalogBonusProduct['category'],
  'size': catalogBonusProduct['default_size'],
  'additional_ingredients': [],
  'picture': catalogBonusProduct['picture'],
  // TODO: fix depends on club card 'default_price_club'
  'price': catalogBonusProduct['default_price']
 };
}

// HTTP Cloud Functions
const app = express();
const api = functions.https.onRequest(app);

app.post('/check_promocode', async (req, res) => {
 const body = JSON.parse(req.body.toString());
 const inputPromocode = body.promo_code;
 const cartPrice = body.cart_price;

 if (inputPromocode == null || cartPrice == null) {
  res.status(400).send('Не заполнены обязательные поля');
  return;
 }

 const promocodeJSON = await
     admin
         .database()
         .ref()
         .child('private')
         .child('promo_codes')
         .child(inputPromocode)
         .once('value');

 if (promocodeJSON == null) {
  res.status(400).send('Неверный промокод');
  return;
 }

 const promocode = promocodeJSON.val();

 if (promocode == null || promocode.from == null || promocode.percent == null) {
  res.status(400).send('Неверный промокод');
  return;
 }

 if (promocode.from > cartPrice) {
  res.status(400).send(`Промокод работает от суммы ${promocode.from}`);
  return;
 }

 res.status(200).send({ percent: promocode.percent, price_from: promocode.from });
});

app.post('/order', async (req, res) => {
 const shouldPost = req.query.post;
 if (shouldPost == null) {
  res.sendStatus(406);
  return;
 }

 const order: Order = JSON.parse(req.body.toString());

 console.log(order);

 if (order.goods === null ||
     order.goods.some((good) => good.quantity == null || good.category == null || good.identifier == null) ||

     order.delivery == null ||

     order.delivery.user == null ||
     order.delivery.user.name == null ||
     order.delivery.user.phone == null ||

     order.delivery.address == null ||
     order.delivery.address.address == null ||
     order.delivery.address.okato == null ||

     order.delivery.delivery_time == null ||

     order.payment == null ||

     order.payment.method == null)
     //
     // (order.payment.method === 'cash' && order.payment.cash == null))
  {
  res.status(406).send('Не хватает данных');
  return;
 }

 if (order.payment.method != 'cash'
     && order.payment.method != 'card'
     && order.payment.method != 'apple_pay'
     && order.payment.method != 'google_pay'
     && order.payment.method != 'samsung_pay') {
  res.status(400).send('Поле "payment.method.name" должно быть одним из cash/card/apple_pay/google_pay/samsung_pay');
  return;
 }

 if ((order.bonus_good != null && order.bonus_good.trim() != '') && (order.payment.promo_code != null && order.payment.promo_code.trim() != '')) {
  res.status(400).send('В одном заказе можно использовать только 1 бонус или промокод');
  return;
 }

 const catalogJSON = await
     admin
         .database()
         .ref()
         .once('value');

 const catalogRoot = catalogJSON.val();

 const privateRoot = catalogRoot.private;
 const catalogPromocodes: Map<string, any> = privateRoot.promo_codes;

 const publicRoot = catalogRoot.public;
 const catalogProducts: Map<string, any> = publicRoot.products;
 const catalogIngredients: Map<string, any> = publicRoot.ingredients;
 const catalogBonusProducts: Map<string, any> = publicRoot.bonus_products;

 const deliveryZones: any[] = publicRoot.delivery_zones;

 const zone = deliveryZones.find((zone) => order.delivery.address.okato.startsWith(zone.okato));

 if (zone == null) {
  res.status(400).send('Адрес недоступен для доставки');
  return
 }

 console.log(`регион доставки: ${zone.zone}, адрес: ${order.delivery.address}`);

 const moscowTime = getMoscowTime();

 order.goods = order.goods.filter((good) => {
  if (good.quantity < 1) return false;

  const catalogCategory = catalogProducts[good.category];
  if (catalogCategory == null) return false;

  const catalogProduct = catalogCategory[good.identifier];
  if (catalogProduct == null) return false;

  const catalogProductSizes: [any] = catalogProduct.sizes;
  if (catalogProductSizes == null) return false;

  const currentWeekday = moscowTime.getDay() === 0 ? 7 : moscowTime.getDay();

  const productSchedule: [any] = catalogProduct.week_schedule;
  if (productSchedule != null && (productSchedule.length > 0)) {
   const schedule = productSchedule.find((rawSchedule) => {
    if (rawSchedule.weekday !== currentWeekday) return false;

    const startTime: string = rawSchedule.start_time;
    if (startTime == null) return false;

    const endTime: string = rawSchedule.end_time;
    if (endTime == null) return false;

    return moment(moscowTime).isBetween(moment(startTime, "HH:mm"), moment(endTime, "HH:mm"));
   });

   if (schedule == null) {
    return false;
   }

  }

  if (catalogProductSizes.length == 1) {
   return true;
  } else {
   return catalogProductSizes.some((catalogSize) => catalogSize.size === good.size);
  }

 });

 order.goods.forEach((good) => {
  const catalogProduct = catalogProducts[good.category][good.identifier];

  good.name = catalogProduct.name;
  good.picture = catalogProduct.pic;
  const catalogProductSizes: [any] = catalogProduct.sizes;
  let size = catalogProductSizes.find((size) => size.size === good.size);
  if (size == null) size = catalogProductSizes[0];
  good.price = size.price;

  const catalogProductAdds: [any] = catalogProduct.adds;
  if (catalogProductAdds == null) {
   good.additional_ingredients = [];
   return;
  }

  good.additional_ingredients = good.additional_ingredients.filter((ingredient) => {
   return catalogProductAdds.some((catalogAdd) => catalogAdd === ingredient && catalogIngredients[ingredient] != null);
  });

  good.price += good
      .additional_ingredients
      .map((ingredient) => catalogIngredients[ingredient].price)
      .reduce((sum, current) => sum + current, 0);

  good.additional_ingredients = good
      .additional_ingredients
      .map((ingredient) => catalogIngredients[ingredient].name);
 });

 order.goods.forEach((good) => good.price *= good.quantity);

 let productsTotalPrice = order.goods.reduce((sum, good) => sum + good.price, 0);

 if (order.payment.promo_code != null && order.payment.promo_code.trim() != '') {
  const catalogPromocode = catalogPromocodes[order.payment.promo_code.trim()];

  if (catalogPromocode == null) {
   res.status(400).send('Неверный промокод');
   return;
  }

  const fromPrice = parseInt(catalogPromocode.from);
  const percent = parseInt(catalogPromocode.percent);

  if (fromPrice == null || percent == null) {
   res.status(400).send('Неверный промокод');
   return;
  }

  if (fromPrice > productsTotalPrice) {
   res.status(400).send(`Промокод работает от суммы: ${fromPrice}`);
   return;
  }

  order.payment.promo_code_info = { percent: percent, from: fromPrice };

  productsTotalPrice = productsTotalPrice * (100.0 - percent) / 100.0;
 }

 if (productsTotalPrice < zone.min_order_price) {
  res.status(400).send(`Минимальная сумма заказа ${zone.min_order_price} рублей`);
  return;
 }

 let deliveryPrice: number;
 let isDayTime;
 const deliveryTime = order.delivery.delivery_time;
 const splittedDeliveryTime = deliveryTime.split("-");
 if (splittedDeliveryTime.length == 2) {
  isDayTime = moment(splittedDeliveryTime[0], "HH:mm").isBetween(moment("8:00", "HH:mm"), moment("21:59", "HH:mm"));
 } else {
  isDayTime = (moscowTime.getHours() >= 8 && moscowTime.getHours() < 22);
 }

 deliveryPrice = (isDayTime) ? zone.daytime_delivery_price : zone.nighttime_delivery_price;

 const goods = order.goods.map((good) => {
  return {
   'quantity': good.quantity,
   'name': good.name,
   'category': good.category,
   'size': good.size,
   'picture': good.picture,
   'additional_ingredients': good.additional_ingredients,
   'price': good.price
  }});

 if (order.bonus_good != null) {
  console.log('order has bonus good');
  const bonusIdentifier = order.bonus_good;
  console.log('order bonus good identifier ' + bonusIdentifier);
  const catalogBonusProduct = catalogBonusProducts[bonusIdentifier];
  const bonusProduct = getSimpleProductFromBonusProduct(catalogBonusProduct, moscowTime, productsTotalPrice);
  if (bonusProduct != null) {
   console.log('push bonus product to goods array');
   if (bonusProduct === false) {
    res.status(400).send('Не выполнены обязательные условия');
    return;
   }
   const bonusPrice: number = bonusProduct.price;
   if (bonusPrice != null) {
    goods.push(bonusProduct);
    productsTotalPrice += bonusPrice;
   }
  }
 }

 let totalPrice = productsTotalPrice + deliveryPrice;
 if (order.delivery.user.phone == testPhone) {
  totalPrice = 3;
 }

 if (order.payment.method === 'cash' && (order.payment.cash != null && order.payment.cash != -1) && order.payment.cash < totalPrice) {
  res.status(400).send('Указанная сумма меньше суммы заказа');
  return;
 }

 let orderID;

 if (shouldPost == 1) {
  const isOnlinePurchase =
      order.payment.method === 'apple_pay' ||
      order.payment.method === 'google_pay' ||
      order.payment.method === 'samsung_pay';

  const record = {
   'goods': JSON.parse(JSON.stringify(goods)),
   'confirmed': !isOnlinePurchase,
   'delivery': order.delivery,
   'payment': order.payment,
   'comment': order.comment,
   'delivery_price': deliveryPrice,
   'total_price': parseInt(`${totalPrice}`)
  };

  const ordersRef = db.collection('orders').doc('orders');
  const ordersRefList = ordersRef.collection('orders');

  orderID = await db.runTransaction(function (transaction) {
   return transaction.get(ordersRef).then(async function (orders) {
    const newOrderID = orders.data().order_id + 1;
    const newOrderRef = ordersRefList.doc(`${newOrderID}`);
    await transaction.set(ordersRef, { order_id: newOrderID });
    await transaction.set(newOrderRef, record);
    return newOrderID;
   });
  });

  if (!isOnlinePurchase) {
   await sendMailForConfirmedOrder(record, orderID);
  }
 }

 const result = {
  'goods': goods,
  'delivery_price': deliveryPrice,
  'total_price': parseInt(`${totalPrice}`),
  'order_id': `${orderID}`
 };

 res.status(200).json(result);
});

async function sendMailForConfirmedOrder(order: any, orderID: String) {
 let body = '';

 body += 'Товары:\n';
 const goods = order.goods.map((good) => {
  let goodString = '';
  const additionalIngredients = good.additional_ingredients || [];
  goodString += `${good.name} ${good.size || ''}\n${good.category}\nКоличество: ${good.quantity} шт`;
  if (additionalIngredients.length !== 0) {
   goodString += `\n${additionalIngredients.join(', ')}`
  }
  return goodString;
 });

 goods.forEach((good, index) => {
  body += `${index + 1}\n${good}\n`;
 });

 const user = order.delivery.user;
 body += '\nПокупатель:\n';
 body += `имя: ${user.name}, телефон: ${user.phone}`;
 if (user.email != null && user.email != '') {
  body += `, e-mail: ${user.email}`;
 }
 body += '\n';

 const address = order.delivery.address;
 body += 'Адрес:\n';
 let addressArray: string[] = [];
 if (address.subway != null && address.subway.trim() != '') {
  addressArray.push(`метро: ${address.subway.trim()}`);
 }
 addressArray.push(`${address.address.trim()}`);
 if (address.porch != null && address.porch.trim() != '') {
  addressArray.push(`подъезд: ${address.porch.trim()}`);
 }
 if (address.floor != null && address.floor.trim() != '') {
  addressArray.push(`этаж: ${address.floor.trim()}`);
 }
 if (address.intercom != null && address.intercom.trim() != '') {
  addressArray.push(`домофон: ${address.intercom.trim()}`);
 }
 if (address.app != null && address.app.trim() != '') {
  addressArray.push(`квартира: ${address.app.trim()}`);
 }
 body += addressArray.join(", ");

 const paymentMethod = order.payment.method;
 body += '\n\nСпособ оплаты: ';
 if (paymentMethod === 'cash') {
  body += `наличными курьеру`;
  if (order.payment.cash != null && order.payment.cash != -1) {
   body += ` (сдача с ${order.payment.cash})`;
  }
 } else if (paymentMethod === 'card') {
  body += 'картой курьеру'
 } else {
  body += 'онлайн-оплата в приложении';
 }
 body += '\n';

 const appliedPromocode = order.payment.promo_code_info;
 if (appliedPromocode != null) {
  body += `Применен промокод ${order.payment.promo_code} (${appliedPromocode.percent}% от ${appliedPromocode.from} руб.)\n`;
 }

 if (order.comment != null && order.comment.trim() != '') {
  body += `Комментарий: ${order.comment}\n\n`;
 }

 body += `Доставка к: ${order.delivery.delivery_time.toLowerCase()}\n`;
 body += `Стоимость доставки: ${order.delivery_price} руб.\n`;
 body += `Общая цена (с доставкой): ${order.total_price} руб.\n`;

 const mailOptions = {
  from: '"МосПицца"',
  to: 'inbox@mos.pizza',
  subject: `Новый заказ № ${orderID}`,
  text: body
 };

 try {
  return mailTransport.sendMail(mailOptions);
 } catch (error) {
  console.error('There was an error while sending the email:', error);
 }
}

app.post('/acquiring/notification/input', async (req, res) => {
 console.log(req.body);
 const receipt = req.body;

 let ordered = {};
 ordered['Password'] = acquiringPassword;
 Object.keys(receipt).forEach(function(key) {
  if (key !== 'Token') {
   ordered[key] = receipt[key];
  }
 });

 const concatenatedValues = Object.keys(ordered).sort().map((key) => ordered[key]).join("");
 const calculatedToken = sha256(concatenatedValues);

 console.log(`ordered: ${ordered}`);
 console.log(`values: ${concatenatedValues}`);
 console.log(`calculated: ${calculatedToken}, received: ${receipt.Token}`);

 if (calculatedToken !== receipt.Token) {
  res.sendStatus(400);
  return;
 }

 const status = receipt.Status;

 const orderRef = db.collection('orders').doc('orders').collection('orders').doc(receipt.OrderId);

 await db.runTransaction(async function (transaction) {
  if (status === 'CONFIRMED') {
   await transaction.update(orderRef, {'confirmed': true});
  } else if (status === 'REVERSED' || status === 'REJECTED') {
   await transaction.delete(orderRef);
  }
 });

 if (status === 'CONFIRMED') {
  await orderRef.get().then(async (doc) => {
   console.log(doc.data());
   await sendMailForConfirmedOrder(doc.data(), receipt.OrderId);
  });

 }

 res.status(200).send('OK');
});

app.post('/acquiring/notification/input_test_terminal', async (req, res) => {
 console.log(req.body);
 const receipt = req.body;

 let ordered = {};
 ordered['Password'] = 'rae62504wz25q4w6';
 Object.keys(receipt).forEach(function(key) {
  if (key !== 'Token') {
   ordered[key] = receipt[key];
  }
 });

 const concatenatedValues = Object.keys(ordered).sort().map((key) => ordered[key]).join("");
 const calculatedToken = sha256(concatenatedValues);

 console.log(`ordered: ${ordered}`);
 console.log(`values: ${concatenatedValues}`);
 console.log(`calculated: ${calculatedToken}, received: ${receipt.Token}`);

 if (calculatedToken !== receipt.Token) {
  res.sendStatus(400);
  return;
 }

 res.status(200).send('OK');
});

module.exports = {
 api
};