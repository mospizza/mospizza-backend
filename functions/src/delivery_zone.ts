export class DeliveryZone {
    constructor(
        public zone: String,
        public min_order_price: number,
        public daytime_delivery_price: number,
        public nighttime_delivery_price: number,
        public okato: string
    ) {
    };
}