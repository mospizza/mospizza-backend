export interface Good {
    quantity: number;
    category: string;
    identifier: string;
    size: string;
    additional_ingredients: string[];
    // my output fields
    name: string;
    price: number;
    picture: string;
}

export interface User {
    uid: string;
    name: string;
    phone: string;
    email: string;
}

export interface Address {
    subway: string;
    address: string;
    okato: string;
    porch: string;
    floor: string;
    intercom: string;
    app: string;
}

export interface Delivery {
    user: User;
    address : Address;
    delivery_time: string;
}

export interface Payment {
    method: string;
    cash: number;
    promo_code: string;
    promo_code_info: { percent: number, from: number };
}

export interface Order {
    goods: Good[];
    bonus_good: string;
    delivery: Delivery;
    payment: Payment;
    comment: string;
}
